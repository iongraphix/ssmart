/* global angular, document, window */
'use strict';


angular.module('starter.controllers', [])

.controller('AppCtrl', function(locate,$scope,$rootScope,$state, $ionicModal, $ionicPopover, $timeout,$ionicHistory) {
    $scope.isExpanded = false;
    $scope.hasHeaderFabLeft = false;
    $scope.hasHeaderFabRight = false;
    $scope.app =  {
        name: "Smart Maid",
        version: "1.0",
        author: "Dominic Damoah"
    }
    // demo data
    $scope.data = locate.getData();
    $scope.chooseCat = function(i){
        $scope.category_modal.hide();
        var places = $scope.data[i].places;
        map.clear();
        var markers = [{
            position: HOME,
            title: "You are here"
        }];
        // Bind markers
        for (var i = 0; i < markers.length; i++) {
            map.addMarker({
                'title': markers[i].title,
                'position': markers[i].position
            }, function(marker) {
                marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
                marker.showInfoWindow();
            });
        }
        var bounds = [];
        bounds.push(markers[0].position);
        for (var i = places.length - 1; i >= 0; i--) {
            
            bounds.push(places[i].code);
            map.addMarker({
              'position': places[i].code,
              'title': places[i].name,
              'icon': '#3b7aff',
              'snippet': places[i].desc,
              'index': i
            }, function(marker) {
              marker.showInfoWindow();
              marker.addEventListener(plugin.google.maps.event.INFO_CLICK, function() {
                alert("InfoWindow is clicked with index "+marker.get("index"));
              });
            });
        }

        map.animateCamera({
          'target': bounds,
          'tilt': 40,   // ignored
          'zoom': 18,   // ignored
          'bearing': 0  // ignored
        }, function() {

        });

    };

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    $ionicModal.fromTemplateUrl('templates/dialogs/show-category.html', {
    scope: $scope
    }).then(function(modal) {
        $scope.category_modal = modal;
    });

    $ionicPopover.fromTemplateUrl('templates/popovers/home-pop.html', {
    scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });


    $scope.showPop = function(e){
        map.setClickable( false );
        $scope.popover.show(e);
    }
    $scope.$on('popover.hidden', function() {
        map.setClickable( true );
    });

    $scope.showCategories = function () {
       map.setClickable( false );
       
       //angular.element(document.getElementById('main-nav')).scope().sayYeah();
        $scope.category_modal.show();
    };
    $scope.$on('modal.hidden', function() {
        map.setClickable( true );
    });

    $scope.sayYeah  = function(){
        alert("Say Yeah Man");
    };
    $scope.closeChooser = function(){
        $scope.category_modal.hide();
    }
    $scope.getCurrentLocation = function(){
          $ionicHistory.nextViewOptions({
             disableBack: true
          });
          $scope.popover.hide();
          $state.go("app.home");
          map.animateCamera({
              'target': HOME,
              'tilt': 60,
              'zoom': 18,
              'bearing': 140,
              'duration': 5000 // 10 seconds
            });
    };

    $scope.gotoAbout = function(){
        $scope.popover.hide();
        $state.go("app.about");
    };

    $scope.gotoDonate = function(){
        $scope.popover.hide();
        $state.go("app.donate");
    };


    ////////////////////////////////////////
    // Layout Methods
    ////////////////////////////////////////

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }

    };

    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    $scope.clearFabs = function() {
        var fabs = document.getElementsByClassName('button-fab');
        if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
        }
    };
})
.controller('ProfileCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    // Set Motion
    $timeout(function() {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 300);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideInRight({
            startVelocity: 3000
        });
    }, 700);

    // Set Ink
    ionicMaterialInk.displayEffect();
})

.controller('HomeCtrl', function($scope,$rootScope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    $scope.$parent.showHeader();
   // $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('AboutCtrl', function($scope,$rootScope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    $scope.$parent.showHeader();
   // $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('DonateCtrl', function($scope,$rootScope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    $scope.$parent.showHeader();
   // $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
});;
