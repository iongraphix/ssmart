/* global angular, document, window */
'use strict';

angular.module('starter.services', [])
.service('locate', function($rootScope){
    this.getData = function(){
        return [ 
        { 
            name: "Mechanics", 
            icon: "ion-model-s", 
            places: [ 
                { 
                    name: "Valley View University Fitting Shop", 
                    desc:"Valley Fitting Shop", 
                    code: { lat:5.798095,lng:-0.124031 },
                    contact: "0545675123"  
                },
                { 
                    name: "Bob's Fitting Shop", 
                    desc:"Bobby Browns Fitting Shop", 
                    code: { lat:5.792299,lng:-0.129374 },
                    contact: "0545242424"  
                }
            ]
        },
        { 
            name: "Police Station", 
            icon: "ion-android-person", 
            places: [ 
                { 
                    name: "Valley View Security", 
                    desc:"Law Enforncement", 
                    code: { lat:5.799209,lng:-0.126311 },
                    contact: "191"  
                },
                { 
                    name: "Dodowa Police", 
                    desc:"Law Enforncement", 
                    code: { lat:5.883094,lng:-0.097827 },
                    contact: "191"  
                }
            ]
        },
        { 
            name: "Fire Services", 
            icon: "ion-flame", 
            places: [ 
                { 
                    name: "Dodowa Fireservice", 
                    desc:"Fire Station Dodowa", 
                    code: { lat:12,lng:12 },
                    contact: "0545242424"  
                },
                { 
                    name: "Valley View Firestation", 
                    desc:"Valley Firestation", 
                    code: { lat:5.795624,lng:-0.119904 },
                    contact: "0543532343"  
                }
            ]
        }
    ];
    
    };
});
