var map;
var HOME = { lat: 5.796226, lng: -0.122257};
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services', 'ionic-material', 'ionMdInput'])

.run(function($ionicPlatform,$rootScope,locate) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }


    var div = document.getElementById("map");
    // Invoking Map using Google Map SDK v2 by dubcanada
    map = plugin.google.maps.Map.getMap(div,{
        'camera': {
            'latLng': setPosition(HOME.lat, HOME.lng),
            'zoom': 17
        }
    });
   // map.setClickable( false );
    map.addEventListener(plugin.google.maps.event.MAP_READY, function(){

        // Defining markers for demo
        var markers = [{
            position: setPosition(HOME.lat, HOME.lng),
            title: "You are here"
        }];
        // Bind markers
        for (var i = 0; i < markers.length; i++) {
            map.addMarker({
                'title': markers[i].title,
                'position': markers[i].position
            }, function(marker) {
                marker.showInfoWindow();
            });
        }
    });
    

    function setPosition(lat, lng) {
        return new plugin.google.maps.LatLng(lat, lng);
    }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })
    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })
    .state('app.about', {
        url: '/about',
        views: {
            'menuContent': {
                templateUrl: 'templates/about.html',
                controller: 'AboutCtrl'
            }
        }
    })
    .state('app.donate', {
        url: '/donate',
        views: {
            'menuContent': {
                templateUrl: 'templates/donate.html',
                controller: 'DonateCtrl'
            }
        }
    })
    .state('app.detail', {
        url: '/detail',
        views: {
            'menuContent': {
                templateUrl: 'templates/detail.html',
                controller: 'DetailCtrl'
            },
            'fabContent': {
                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                controller: 'DetailCtrl'
            }
        }
    })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});
